﻿using System.Web;
using System.Web.Mvc;
using GerenciadorFinanceiro.Filtros;

namespace GerenciadorFinanceiro
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //filters.Add(new AutorizacaoFilterAttribute());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using GerenciadorFinanceiro.Models;
using GerenciadorFinanceiro.Services;
using WebMatrix.WebData;

namespace GerenciadorFinanceiro.Controllers
{
    public class UsuarioController : Controller
    {
        private readonly UsuarioService usuarioService = new UsuarioService();
        // GET: Usuario
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Novo()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Adiciona(UsuarioViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    WebSecurity.CreateUserAndAccount(model.Email, model.Senha, new {Nome = model.Nome});
                    return RedirectToAction("Index", "Login");
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("usuario.Invalido", e.Message);
                    return View("Novo", model);
                }
            }
            else
            {
                return View("Novo", model);
            }
        }
    }
}
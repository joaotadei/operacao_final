﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GerenciadorFinanceiro.Models;
using WebMatrix.WebData;

namespace GerenciadorFinanceiro.Controllers
{
    
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Autentica(string email, string senha)
        {
            if (WebSecurity.Login(email, senha))
            {
                return RedirectToAction("Index", "Transacao");
            }
            else
            {
                ModelState.AddModelError("login.Invalido", "Login ou senha incorretos");
                return View("Index");
            }

            //Usuario usuario = usuarioService.GetUsuario(login, senha);
            //if (usuario != null)
            //{
            //    Session["usuarioLogado"] = usuario;
            //    return RedirectToAction("Index", "Transacao");
            //}
        }

        public ActionResult Logout()
        {
            WebSecurity.Logout();
            return RedirectToAction("Index");
        }

    }
}
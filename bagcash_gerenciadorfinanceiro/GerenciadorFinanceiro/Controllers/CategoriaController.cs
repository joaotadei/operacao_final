﻿using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.Security;
using GerenciadorFinanceiro.Models;
using GerenciadorFinanceiro.Services;

namespace GerenciadorFinanceiro.Controllers
{
    public class CategoriaController : Controller
    {
        private readonly CategoriaService categoriaService = new CategoriaService();
        private readonly TransacaoService transacaoService = new TransacaoService();
        int userId = Convert.ToInt32(Membership.GetUser().ProviderUserKey);

        public ActionResult Index()
        {
            var categorias = categoriaService.GetAll();
            return View(categorias.ToList());
        }

        public ActionResult Create()
        {
            ViewBag.CategoriaId = new SelectList(categoriaService.GetAll(), "CategoriaId", "Descricao", new Categoria().CategoriaId);

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Categoria categoria)
        {
            if (ModelState.IsValid)
            {
                categoria.UsuarioId = userId;
                categoriaService.Add(categoria);
                return RedirectToAction("Index");
            }
            return PartialView(categoria);
        }

        public ActionResult Edit(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Categoria categoria = categoriaService.GetById(id);
            if (categoria == null)
            {
                return HttpNotFound();
            }
            return View(categoria);
        }

        [HttpPost]
        public ActionResult Edit(Categoria categoria)
        {
            if (ModelState.IsValid)
            {
                categoriaService.Update(categoria);
                return RedirectToAction("Index");
            }
            return View(categoria);
        }

        public ActionResult Delete(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Categoria categoria = categoriaService.GetById(id);
            if (categoria == null)
            {
                return HttpNotFound();
            }
            return View(categoria);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Categoria categoria = categoriaService.GetById(id);
            categoriaService.Remove(categoria);
            return RedirectToAction("Index");
        }

        //public ActionResult CategoriasQuantificadas()
        //{
        //    var categoriasQuantificadas = new List<KeyValuePair<string, decimal>>();
        //    var categorias = db.Categorias;
        //    foreach (var item in categorias)
        //    {
        //        decimal valorTotalCategoria = db.Despesas.Where(x => x.CategoriaId == item.CategoriaId && x.DataVencimento.Month == DateTime.Now.Month).Sum(x => (decimal?)x.Valor) ?? 0;
        //        if (valorTotalCategoria > 0)
        //            categoriasQuantificadas.Add(new KeyValuePair<string, decimal>(item.Descricao, valorTotalCategoria));
        //    }
        //    return Json(categoriasQuantificadas, JsonRequestBehavior.AllowGet);
        //}

        public ActionResult PieChart()
        {
            var categorias = categoriaService.GetAll();

            var resultado = categorias.Select(x => new
            {
                label = x.Descricao,
                value =  transacaoService.GetAll().Where(y => y.CategoriaId == x.CategoriaId && y.DataTransacao.Month == DateTime.Now.Month).Sum(y => (decimal?)y.Valor) ?? 0
            }).ToList();

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            categoriaService.Dispose();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Services;
using System.Web.Security;
using System.Web.Services;
using GerenciadorFinanceiro.Contexto;
using GerenciadorFinanceiro.Models;
using GerenciadorFinanceiro.Services;

namespace GerenciadorFinanceiro.Controllers
{
    [Authorize]
    public class TransacaoController : Controller
    {
        private readonly TransacaoService transacaoService = new TransacaoService();
        private readonly CategoriaService categoriaService = new CategoriaService();
        int userId = Convert.ToInt32(Membership.GetUser().ProviderUserKey);

        // GET: Transacao
        public ActionResult Index()
        {

            ViewBag.SaldoDoMes = transacaoService.Saldo(userId);
            ViewBag.DespesasDoMes = transacaoService.DespesasDoMes(userId);
           IEnumerable <Transacao> transacoes = new List<Transacao>();
            transacoes = transacaoService.TransacoesRecentes(userId);
            return View(transacoes);
        }
        //RECEITA
        public ActionResult NovaReceita()
        {
            ViewBag.CategoriaId = new SelectList(categoriaService.CategoriasPorUsuario(userId), "CategoriaId", "Descricao");
            return View();
        }

        [HttpPost]
        public ActionResult NovaReceita(Transacao transacao)
        {
            
            transacao.TipoTransacao = TipoTransacao.Entrada;
            transacao.DataTransacao = DateTime.Now;
            transacao.UsuarioId = userId;
            if (ModelState.IsValid)
            {
                transacaoService.Add(transacao);
                return RedirectToAction("Index");
            }
            return View(transacao);
        }

        // DESPESA
        public ActionResult NovaDespesa()
        {
            ViewBag.CategoriaId = new SelectList(categoriaService.CategoriasPorUsuario(userId), "CategoriaId", "Descricao");
            return View();
        }

        [HttpPost]
        public ActionResult NovaDespesa(Transacao transacao)
        {
            transacao.TipoTransacao = TipoTransacao.Saída;
            transacao.DataTransacao = DateTime.Now;
            transacao.UsuarioId = userId;
            if (ModelState.IsValid)
            {
                transacaoService.Add(transacao);
                return RedirectToAction("Index");
            }
            return View(transacao);
        }

        // GET : EXTRATO DE TRANSAÇÕES
        public ActionResult ExtratoTransacoes()
        {
            ViewBag.CategoriaId = new SelectList(categoriaService.CategoriasPorUsuario(userId), "CategoriaId", "Descricao");
            ViewBag.SaldoDoMes = transacaoService.Saldo(userId);
            ViewBag.DespesasDoMes = transacaoService.DespesasDoMes(userId);
            ViewBag.ReceitasDoMes = transacaoService.ReceitasDoMes(userId);
           
            return View();
        }

        public ActionResult BuscaExtratoDeTransacoes(DateTime data1, DateTime data2, TipoTransacao tipo, int categoria, bool status)
        {
            IEnumerable<Transacao> extrato = transacaoService.ExtratoDeTransacoes(status, data1, data2, tipo, categoria, userId);
            ViewBag.DataInicio = data1;
            ViewBag.DataFim = data2;
            return PartialView("_ExtratoTransacoesList", extrato);
        }

        // GET SUAS TRANSAÇÃOES, PARA EDICAO
        public ActionResult SuasTransacoes()
        {
            IEnumerable<Transacao> transacoes = transacaoService.GetAllById(userId);
            return View(transacoes);
        }

        public ActionResult EditarTransacoes()
        {
            return View();
        }

        // DADOS DO GRAFICO INDEX TRANSAÇÕES
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public JsonResult PieChart()
        {
            ICollection<Categoria> categorias = categoriaService.CategoriasPorUsuario(userId);
            
            var chartData = new object[categorias.Count + 1];
            chartData[0] = new object[]
            {
                "Categoria",
                "Valor"
            };
            int j = 0;

            foreach (var item in categorias)
            {
                decimal valorTotalCategoria = transacaoService.DespesasDoMesPorCategoria(item.CategoriaId);
                j++;
                if (valorTotalCategoria > 0)
                {
                    
                    chartData[j] = new object[] { item.Descricao, valorTotalCategoria };
                }
                else
                {
                    chartData[j] = new object[] { item.Descricao, 0 };
                }
            }
            return Json(chartData, JsonRequestBehavior.AllowGet);

        }

        // DESPESAS PENDENTES
        // GET:
        public ActionResult DespesasPendentes()
        {
            IEnumerable<Transacao> pendentes = transacaoService.DespesasPendentes(userId);
            return View(pendentes);
        }

        [HttpPost]
        public ActionResult DespesasPendentes(Transacao transacao)
        {
            return View();
        }

        [HttpPost]
        public ActionResult EfetivarTransacao(int id)
        {
            Transacao transacao = transacaoService.GetById(id);
            transacao.Status = true;
            transacao.DataTransacao = DateTime.Now;
            if (ModelState.IsValid)
            {
                transacaoService.Update(transacao);
                return RedirectToAction("Index", "Transacao");
            }
            return View();
            
        }
    }
}
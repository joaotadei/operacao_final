﻿using System.ComponentModel.DataAnnotations.Schema;
using GerenciadorFinanceiro.Models;
using System.Data.Entity.ModelConfiguration;

namespace GerenciadorFinanceiro.EntityConfig
{
    public class CategoriaConfig : EntityTypeConfiguration<Categoria>
    {
        public CategoriaConfig()
        {
            ToTable("Categoria");
            HasKey(c => c.CategoriaId);
            Property(c => c.CategoriaId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(c => c.Descricao).HasMaxLength(30).IsRequired();
        }
    }
}
﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using GerenciadorFinanceiro.Models;

namespace GerenciadorFinanceiro.EntityConfig
{
    public class EncargoConfig : EntityTypeConfiguration<Encargo>
    {
        public EncargoConfig()
        {
            ToTable("Encargo");
            HasKey(e => e.EncargoId);
            Property(e => e.EncargoId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }
    }
}
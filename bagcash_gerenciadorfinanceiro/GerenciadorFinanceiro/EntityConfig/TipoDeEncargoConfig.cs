﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using GerenciadorFinanceiro.Models;

namespace GerenciadorFinanceiro.EntityConfig
{
    public class TipoDeEncargoConfig : EntityTypeConfiguration<TipoDeEncargo>
    {
        public TipoDeEncargoConfig()
        {
            ToTable("Transacao");
            HasKey(t => t.TipoDeEncargoId);
            Property(t => t.TipoDeEncargoId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }
    }
}
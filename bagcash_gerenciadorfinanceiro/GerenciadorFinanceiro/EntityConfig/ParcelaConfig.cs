﻿using System.ComponentModel.DataAnnotations.Schema;
using GerenciadorFinanceiro.Models;
using System.Data.Entity.ModelConfiguration;

namespace GerenciadorFinanceiro.EntityConfig
{
    public class ParcelaConfig : EntityTypeConfiguration<Parcela>
    {
        public ParcelaConfig()
        {
            ToTable("Parcela");
            HasKey(p => p.ParcelaId);
            Property(p => p.ParcelaId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }
    }
}
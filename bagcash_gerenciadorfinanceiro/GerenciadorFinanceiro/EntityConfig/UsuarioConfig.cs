﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using GerenciadorFinanceiro.Models;

namespace GerenciadorFinanceiro.EntityConfig
{
    public class UsuarioConfig : EntityTypeConfiguration<Usuario>
    {
        public UsuarioConfig()
        {
            ToTable("Usuario");
            HasKey(u => u.UsuarioId);
            Property(u => u.UsuarioId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(u => u.Nome).HasMaxLength(30).IsRequired();
        }
    }
}
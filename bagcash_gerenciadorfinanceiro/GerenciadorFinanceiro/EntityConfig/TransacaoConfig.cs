﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using GerenciadorFinanceiro.Models;

namespace GerenciadorFinanceiro.EntityConfig
{
    public class TransacaoConfig : EntityTypeConfiguration<Transacao>
    {
        public TransacaoConfig()
        {
            ToTable("Transacao");
            HasKey(t => t.TransacaoId);
            Property(t => t.TransacaoId ).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using GerenciadorFinanceiro.Contexto;

namespace GerenciadorFinanceiro.Repository
{
    public class RepositoryBase<TEntity> : IDisposable where TEntity : class 
    {
        protected readonly DBContext Db = new DBContext();

        public void Add(TEntity obj)
        {
            Db.Set<TEntity>().Add(obj);
            Db.SaveChanges();
        }

        public void Update(TEntity obj)
        {
            Db.Entry(obj).State = EntityState.Modified;
            Db.SaveChanges();
        }

        public void Remove(TEntity obj)
        {
            Db.Set<TEntity>().Remove(obj);
            Db.SaveChanges();
        }

        public TEntity GetById(int id)
        {
            return Db.Set<TEntity>().Find(id);
        }

        public ICollection<TEntity> GetAll()
        {
            return Db.Set<TEntity>().ToList();
        }

        public void Dispose()
        {
            Db.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
﻿using GerenciadorFinanceiro.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GerenciadorFinanceiro.Repository
{
    public class CategoriaRepository : RepositoryBase<Categoria>
    {
       
        public ICollection<Categoria> CategoriasPorUsuario(int id)
        {
            return Db.Categorias.Where(c => c.UsuarioId == id).ToList();
        }
    }


}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using GerenciadorFinanceiro.Contexto;
using GerenciadorFinanceiro.Models;

namespace GerenciadorFinanceiro.Repository
{
    public class TransacaoRepository : RepositoryBase<Transacao>
    {
        public decimal Saldo(int id)
        {
            return ReceitasDoMes(id) - DespesasDoMes(id);
        }

        public decimal ReceitasDoMes(int id)
        {
            return Db.Transacoes.Where(t => t.DataVencimento.Month == DateTime.Now.Month).Where(t => t.TipoTransacao == TipoTransacao.Entrada && t.Usuario.UsuarioId == id).Sum(t => (decimal?)t.Valor) ?? 0;
        }

        public decimal DespesasDoMes(int id)
        {
           return Db.Transacoes.Where(t => t.DataVencimento.Month == DateTime.Now.Month).Where(t => t.TipoTransacao == TipoTransacao.Saída && t.Usuario.UsuarioId == id).Sum(t => (decimal?)t.Valor) ?? 0;
        }

        public IEnumerable<Transacao> TransacoesRecentes(int Id)
        {
            return Db.Transacoes.OrderByDescending(t => t.Usuario.UsuarioId == Id).Take(6);
        }

        public IEnumerable<Transacao> ExtratoDeTransacoes(bool status, DateTime inicio, DateTime fim, TipoTransacao tipo, int categoria, int userId)
        {
            return Db.Transacoes.Where(t => t.Status == status && DbFunctions.TruncateTime(t.DataTransacao) >= inicio.Date && DbFunctions.TruncateTime(t.DataTransacao) <= fim.Date && t.TipoTransacao == tipo && t.CategoriaId == categoria && t.UsuarioId == userId);
        }


        public decimal DespesasDoMesPorCategoria(int categoriaId)
        {
            return Db.Transacoes.Where(t => t.DataVencimento.Month == DateTime.Now.Month && t.TipoTransacao == TipoTransacao.Saída && t.CategoriaId == categoriaId).Sum(t => (decimal?)t.Valor) ?? 0;
        }

        public IEnumerable<Transacao> GetAllById(int userId)
        {
           return Db.Transacoes.Where(t => t.UsuarioId == userId);
        }

        internal void GetAllById(int v, int userId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Transacao> DespesasPendentes(int Id)
        {
            return Db.Transacoes.Where(t => t.TipoTransacao == TipoTransacao.Saída && t.Status == false && t.UsuarioId == Id);
        }
    }
}
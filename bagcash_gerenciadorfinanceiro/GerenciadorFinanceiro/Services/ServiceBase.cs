﻿using System;
using System.Collections.Generic;
using GerenciadorFinanceiro.Repository;

namespace GerenciadorFinanceiro.Services
{
    public class ServiceBase<TEntity> : IDisposable where TEntity : class 
    {
        protected readonly RepositoryBase<TEntity> repositoryBase = new RepositoryBase<TEntity>();

        public void Add(TEntity obj)
        {
            repositoryBase.Add(obj);
        }

        public void Update(TEntity obj)
        {
            repositoryBase.Update(obj);
        }

        public void Remove(TEntity obj)
        {
            repositoryBase.Remove(obj);
        }

        public TEntity GetById(int id)
        {
            return repositoryBase.GetById(id);
        }

        public ICollection<TEntity> GetAll()
        {
            return repositoryBase.GetAll();
        }

        public void Dispose()
        {
            repositoryBase.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
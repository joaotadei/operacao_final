﻿using System;
using System.Collections.Generic;
using GerenciadorFinanceiro.Contexto;
using GerenciadorFinanceiro.Models;
using GerenciadorFinanceiro.Repository;

namespace GerenciadorFinanceiro.Services
{
    public class TransacaoService : ServiceBase<Transacao>
    {
        TransacaoRepository transacaoRepository = new TransacaoRepository();

        public decimal Saldo(int id)
        {
            return ReceitasDoMes(id) - DespesasDoMes(id);
        }

        public decimal ReceitasDoMes(int id)
        {
            return transacaoRepository.ReceitasDoMes(id);
        }

        public decimal DespesasDoMes(int id)
        {
            return transacaoRepository.DespesasDoMes(id);
        }

        public IEnumerable<Transacao> TransacoesRecentes(int Id)
        {
            return transacaoRepository.TransacoesRecentes(Id);
        }

        public IEnumerable<Transacao> ExtratoDeTransacoes(bool status, DateTime data1, DateTime data2, TipoTransacao tipo, int categoria, int userId)
        {
           return transacaoRepository.ExtratoDeTransacoes(status, data1, data2, tipo, categoria, userId);
        }

        public decimal DespesasDoMesPorCategoria(int categoriaId)
        {
            return transacaoRepository.DespesasDoMesPorCategoria(categoriaId);
        }


        public IEnumerable<Transacao> DespesasPendentes(int Id)
        {
            return transacaoRepository.DespesasPendentes(Id);
        }

        internal void EfetivarTransacao(int id)
        {
            transacaoRepository.GetById(id);

        }

        public IEnumerable<Transacao> GetAllById(int userId)
        {
            return transacaoRepository.GetAllById(userId);
        }
    }
}
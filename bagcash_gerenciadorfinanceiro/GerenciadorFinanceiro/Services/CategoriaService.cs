﻿using GerenciadorFinanceiro.Models;
using GerenciadorFinanceiro.Repository;
using System.Collections.Generic;

namespace GerenciadorFinanceiro.Services
{
    public class CategoriaService : ServiceBase<Categoria>
    {

        CategoriaRepository categoriaRepository = new CategoriaRepository();

        public ICollection<Categoria> CategoriasPorUsuario(int Id)
        {
            return categoriaRepository.CategoriasPorUsuario(Id);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using GerenciadorFinanceiro.Models;
using GerenciadorFinanceiro.EntityConfig;

namespace GerenciadorFinanceiro.Contexto
{
    public class DBContext : DbContext
    {
        public DbSet<Transacao> Transacoes{ get; set; }

        public DbSet<Encargo> Encargos { get; set; }

        public DbSet<TipoDeEncargo> TipoDeEncargos { get; set; }

        public DbSet<Usuario> Usuarios { get; set; }

        public DbSet<Categoria> Categorias { get; set; }

        public DbSet<Parcela> Parcelas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Properties()
                .Where(p => p.Name == p.ReflectedType.Name + "Id")
                .Configure(p => p.IsKey());

            modelBuilder.Properties<string>()
                .Configure(p => p.HasColumnType("varchar"));

            modelBuilder.Properties<string>()
                .Configure(p => p.HasMaxLength(100));

            modelBuilder.Configurations.Add(new TransacaoConfig());
            modelBuilder.Configurations.Add(new CategoriaConfig());
            modelBuilder.Configurations.Add(new ParcelaConfig());

            modelBuilder.Entity<Transacao>()
                .Property(t => t.DataVencimento)
                .HasColumnType("datetime");
        }
    }
}
﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using GerenciadorFinanceiro.Filtros;
using WebMatrix.WebData;

namespace GerenciadorFinanceiro
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            //GlobalFilters.Filters.Add(new AutorizacaoFilterAttribute());
            WebSecurity.InitializeDatabaseConnection("DBContext", "Usuario", "UsuarioId", "Email", true);

        }
    }
}

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using GerenciadorFinanceiro.Models;
using GerenciadorFinanceiro.Services;

namespace GerenciadorFinanceiro.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<GerenciadorFinanceiro.Contexto.DBContext>
    {
        private readonly CategoriaService categoriaService = new CategoriaService();

        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(GerenciadorFinanceiro.Contexto.DBContext context)
        {
            IList<Categoria> categorias = new List<Categoria>();
            categorias.Add(new Categoria() { Descricao = "Sa�de" });
            categorias.Add(new Categoria() { Descricao = "Alimenta��o" });
            categorias.Add(new Categoria() { Descricao = "Transporte" });

            foreach (var categoria in categorias)
            {
                categoriaService.Add(categoria);
            }
        }
    }
}

namespace GerenciadorFinanceiro.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class teste : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categoria",
                c => new
                    {
                        CategoriaId = c.Int(nullable: false, identity: true),
                        UsuarioId = c.Int(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 30, unicode: false),
                        CategoriaPai_CategoriaId = c.Int(),
                    })
                .PrimaryKey(t => t.CategoriaId)
                .ForeignKey("dbo.Categoria", t => t.CategoriaPai_CategoriaId)
                .Index(t => t.CategoriaPai_CategoriaId);
            
            CreateTable(
                "dbo.Encargo",
                c => new
                    {
                        EncargoId = c.Int(nullable: false, identity: true),
                        Parcela_ParcelaId = c.Int(),
                        TipoDeEncargo_TipoDeEncargoId = c.Int(),
                    })
                .PrimaryKey(t => t.EncargoId)
                .ForeignKey("dbo.Parcela", t => t.Parcela_ParcelaId)
                .ForeignKey("dbo.TipoDeEncargo", t => t.TipoDeEncargo_TipoDeEncargoId)
                .Index(t => t.Parcela_ParcelaId)
                .Index(t => t.TipoDeEncargo_TipoDeEncargoId);
            
            CreateTable(
                "dbo.Parcela",
                c => new
                    {
                        ParcelaId = c.Int(nullable: false, identity: true),
                        TransacaoId = c.Int(nullable: false),
                        Valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DataVencimento = c.DateTime(nullable: false),
                        DataPagamento = c.DateTime(nullable: false),
                        Pago = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ParcelaId)
                .ForeignKey("dbo.Transacao", t => t.TransacaoId)
                .Index(t => t.TransacaoId);
            
            CreateTable(
                "dbo.Transacao",
                c => new
                    {
                        TransacaoId = c.Int(nullable: false, identity: true),
                        Descricao = c.String(maxLength: 100, unicode: false),
                        DataTransacao = c.DateTime(nullable: false),
                        TipoTransacao = c.Int(nullable: false),
                        UsuarioId = c.Int(nullable: false),
                        CategoriaId = c.Int(nullable: false),
                        SaldoAtual = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DataVencimento = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.TransacaoId)
                .ForeignKey("dbo.Categoria", t => t.CategoriaId)
                .ForeignKey("dbo.Usuario", t => t.UsuarioId)
                .Index(t => t.UsuarioId)
                .Index(t => t.CategoriaId);
            
            CreateTable(
                "dbo.Usuario",
                c => new
                    {
                        UsuarioId = c.Int(nullable: false, identity: true),
                        Nome = c.String(maxLength: 100, unicode: false),
                        Email = c.String(maxLength: 100, unicode: false),
                    })
                .PrimaryKey(t => t.UsuarioId);
            
            CreateTable(
                "dbo.TipoDeEncargo",
                c => new
                    {
                        TipoDeEncargoId = c.Int(nullable: false, identity: true),
                        Descricao = c.String(maxLength: 100, unicode: false),
                        PorcentagemEncargo = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ValorEncargo = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.TipoDeEncargoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Encargo", "TipoDeEncargo_TipoDeEncargoId", "dbo.TipoDeEncargo");
            DropForeignKey("dbo.Parcela", "TransacaoId", "dbo.Transacao");
            DropForeignKey("dbo.Transacao", "UsuarioId", "dbo.Usuario");
            DropForeignKey("dbo.Transacao", "CategoriaId", "dbo.Categoria");
            DropForeignKey("dbo.Encargo", "Parcela_ParcelaId", "dbo.Parcela");
            DropForeignKey("dbo.Categoria", "CategoriaPai_CategoriaId", "dbo.Categoria");
            DropIndex("dbo.Transacao", new[] { "CategoriaId" });
            DropIndex("dbo.Transacao", new[] { "UsuarioId" });
            DropIndex("dbo.Parcela", new[] { "TransacaoId" });
            DropIndex("dbo.Encargo", new[] { "TipoDeEncargo_TipoDeEncargoId" });
            DropIndex("dbo.Encargo", new[] { "Parcela_ParcelaId" });
            DropIndex("dbo.Categoria", new[] { "CategoriaPai_CategoriaId" });
            DropTable("dbo.TipoDeEncargo");
            DropTable("dbo.Usuario");
            DropTable("dbo.Transacao");
            DropTable("dbo.Parcela");
            DropTable("dbo.Encargo");
            DropTable("dbo.Categoria");
        }
    }
}

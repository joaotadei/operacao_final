﻿using System.Collections;
using System.Collections.Generic;

namespace GerenciadorFinanceiro.Models
{
    public class TipoDeEncargo
    {
        public int TipoDeEncargoId { get; set; }

        public string Descricao { get; set; }

        public decimal PorcentagemEncargo { get; set; }

        public IEnumerable<Encargo> Encargos { get; set; }

        public decimal ValorEncargo { get; set; }
    }
}
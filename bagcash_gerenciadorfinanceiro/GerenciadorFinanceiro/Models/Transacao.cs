﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GerenciadorFinanceiro.Models
{
    public class Transacao
    {
        public int TransacaoId { get; set; }

        public string Descricao { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [DataType(DataType.Date)]
        public DateTime DataTransacao { get; set; }

        public TipoTransacao TipoTransacao { get; set; }

        public Usuario Usuario { get; set; }

        public int UsuarioId { get; set; }

        public Categoria Categoria { get; set; }

        public int CategoriaId { get; set; }

        public decimal SaldoAtual { get; set; }

        public decimal Valor { get; set; }

        [DataType(DataType.Date)]
        public DateTime DataVencimento { get; set; }

        public bool Status { get; set; }

        public IEnumerable<Parcela> Parcelas { get; set; }
    }
}
﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GerenciadorFinanceiro.Models
{
    public class Usuario
    {
        public int UsuarioId { get; set; }

        public string Nome { get; set; }

        public string Email { get; set; }

        public IEnumerable<Transacao> Transacoes { get; set; }
    }
}
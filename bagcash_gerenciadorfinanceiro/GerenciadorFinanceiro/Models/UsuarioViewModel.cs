﻿using System.ComponentModel.DataAnnotations;

namespace GerenciadorFinanceiro.Models
{
    public class UsuarioViewModel
    {
        [Required]
        public string Nome { get; set; }

        [Required, EmailAddress]
        [Display(Name = "E-mail")]
        public string Email { get; set; }

        [Required]
        public string Senha { get; set; }

        [Compare("Senha")]
        [Display(Name = "Confirme sua senha:")]
        public string ConfirmacaoSenha { get; set; }
    }
}
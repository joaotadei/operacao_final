﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GerenciadorFinanceiro.Models
{
    public class Categoria
    {
        public Categoria()
        {
        }

        public int CategoriaId { get; set; }

        public int UsuarioId { get; set; }

        public Categoria CategoriaPai { get; set; }

        [Display(Name = "Categoria")]
        public string Descricao { get; set; }

        public virtual IEnumerable<Transacao> Transacoes { get; set; }
    }
}
﻿using System.Security.AccessControl;

namespace GerenciadorFinanceiro.Models
{
    public class Encargo
    {
        public int EncargoId { get; set; }
        
        public TipoDeEncargo TipoDeEncargo { get; set; }

        public Parcela Parcela { get; set; }

        public Encargo(Parcela parcela)
        {
            Parcela = parcela;
        }
    }
}
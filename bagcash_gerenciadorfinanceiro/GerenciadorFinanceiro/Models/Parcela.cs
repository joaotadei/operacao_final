﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GerenciadorFinanceiro.Models
{
    public partial class Parcela
    {
        public int ParcelaId { get; set; }

        public Transacao Transacao { get; set; }

        public int TransacaoId { get; set; }

        public ICollection<Encargo> Encargos { get; set; }

        public decimal Valor { get; set; }

        [DataType(DataType.Date)]
        public DateTime DataVencimento { get; set; }

        [DataType(DataType.Date)]
        public DateTime DataPagamento { get; set; }

        public bool Pago { get; set; }

        public Parcela(Transacao transacao)
        {
            Transacao = transacao;
        }
    }
}
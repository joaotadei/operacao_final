﻿namespace GerenciadorFinanceiro.Models
{
    public enum TipoTransacao
    {
        Entrada = 1,
        Saída = 2
    }
}